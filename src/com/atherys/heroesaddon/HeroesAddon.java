package com.atherys.heroesaddon;

import ca.xshade.bukkit.questioner.Questioner;
import com.atherys.heroesaddon.commands.*;
import com.atherys.heroesaddon.listeners.*;
import com.herocraftonline.heroes.Heroes;
import com.palmergames.bukkit.towny.Towny;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.HashMap;


public class HeroesAddon extends JavaPlugin {

    public static final int PARTICLE_RANGE = 20;
    public static final int BIG_PARTICLE_RANGE = 40;
	//New command system
	public static HashMap<String, CommandModule> commands;
	public static HeroesAddon instance;
    // Libs that are Used
    private static Heroes heroes = (Heroes) Bukkit.getServer().getPluginManager().getPlugin("Heroes");
    private static Towny towny = (Towny) Bukkit.getServer().getPluginManager().getPlugin("Towny");
    private static PermissionsEx pex = (PermissionsEx) Bukkit.getServer().getPluginManager().getPlugin("PermissionsEx");
    private static Questioner questioner = (Questioner) Bukkit.getServer().getPluginManager().getPlugin("Questioner");
    private static WorldGuardPlugin worldGuard = (WorldGuardPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
    public static Towny getTowny() {
        return towny;
    }

    public static Heroes getHeroes() {
        return heroes;
    }

    public static PermissionsEx getPex() {
        return pex;
    }

    public static WorldGuardPlugin getWorldGuard() {
        return worldGuard;
    }

	public static Questioner getQuestioner(){
		return questioner;
	}

	@Override
    public void onEnable() {
        // Copy the Config Across
        getConfig().options().copyDefaults(true);
        saveConfig();
		//New command system
		instance = this;
		commands = new HashMap<String, CommandModule>();
		registerCommands();

		// Startup the Listeners!
       // getServer().getPluginManager().registerEvents(new ClassSwitch(this), this);
        //getServer().getPluginManager().registerEvents(new CommandListener(this), this);
        //getServer().getPluginManager().registerEvents(new HeroesPartyListener(this), this);
        getServer().getPluginManager().registerEvents(new PetListener(), this);
        getServer().getPluginManager().registerEvents(new HeroesListener(this), this);
    }

    @Override
    public void onDisable() {
	    instance = null;
	    commands.clear();
        System.out.println("[HeroesAddon] disabled!");
    }

	public void registerCommands()
	{
		new herodebugCMD();
		new BindsCMD();
		new classesWithCMD();
		//new CooldownResetCMD();
		//new CooldownResetSubCommand();
		new heroEffectsCMD();
		new nineOneOneCMD();
		new masteriesCMD();
		//new HeroAdminPartylistCMD();
		//Feel free to add more :)
	}
}
