package com.atherys.heroesaddon.listeners;

import com.atherys.heroesaddon.HeroesAddon;
import com.dsh105.echopet.compat.api.plugin.EchoPet;
import com.dsh105.echopet.compat.api.plugin.IPetManager;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created with IntelliJ.
 * User: NeumimTo
 * Date: 19.3.14
 * Time: 21:46
 */
public class PetListener implements Listener {
    private final IPetManager manager;

    public PetListener() {
        manager = EchoPet.getPlugin().getPetManager();
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().toLowerCase().startsWith("/pet") || event.getMessage().toLowerCase().startsWith("/home")) {
            final Player p = event.getPlayer();
            if (HeroesAddon.getHeroes().getCharacterManager().getHero(p).isInCombat()) {
                p.sendMessage(ChatColor.GRAY + "You cannot use this command while in combat");
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onDamage(final EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            final Player d = (Player) event.getDamager();
            final Player p = (Player) event.getEntity();
            manager.removePets(d, false);
            manager.removePets(p, false);
        }
    }
}
