package com.atherys.heroesaddon.listeners;

import com.atherys.heroesaddon.HeroesAddon;
import com.herocraftonline.heroes.api.events.ClassChangeEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.classes.HeroClass;
import com.herocraftonline.heroes.characters.effects.Effect;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ClassSwitch implements Listener {
    private final com.atherys.heroesaddon.HeroesAddon HeroesAddon;

    public ClassSwitch(HeroesAddon HeroesAddon) {
        this.HeroesAddon = HeroesAddon;
    }

    private final void ClassMessage(Hero inhero, String fromclass, String targetclass) {
	    for (Player p : Bukkit.getOnlinePlayers()) {
		    if (!p.equals(inhero.getPlayer()) && p.getWorld().equals(inhero.getPlayer().getWorld())
				    && p.getLocation().distance(inhero.getPlayer().getLocation()) < 32)
			    p.sendMessage(ChatColor.RED + "[ClassChange]" + ChatColor.GREEN + " " + inhero.getName() + " changed class from " + fromclass + " to " + targetclass);
	    }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onClassChange(ClassChangeEvent event) {
	    Hero hero = event.getHero();
	    Player p = hero.getPlayer();
	    long time = System.currentTimeMillis();
	    HeroClass fromclass = event.getFrom();
	    HeroClass targetclass = event.getTo();
	    long cooldown = (HeroesAddon.getConfig().getInt("class-change-cooldown")) * 60000;


	    if (!hero.isMaster(targetclass)) {
	    ClassMessage(hero, fromclass.getName(), targetclass.getName());
	    removeEffects(hero);
	    } else {
		    if (hero.getCooldown("ClassSwitch") == null || hero.getCooldown("ClassSwitch") <= time) {
			    hero.setCooldown("ClassSwitch", time + cooldown);
			    ClassMessage(hero, fromclass.getName(), targetclass.getName());
			    removeEffects(hero);
		    } else {
			    if (event.canCancel()) {
				    p.sendMessage(ChatColor.RED + "Sorry, you still have " + (hero.getCooldown("ClassSwitch") - time) / 1000 + " seconds before you can switch classes!");
				    event.setCancelled(true);
			    }
		    }
	    }
    }

    private void removeEffects(Hero hero) {

	    for (Effect effect : hero.getEffects()) {
		    hero.removeEffect(effect);
	    }
	    Player p = hero.getPlayer();
	    p.setHealth(5);
	    p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 1));
	    p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 20));
	    p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 20));

    }

}
