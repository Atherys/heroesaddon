package com.atherys.heroesaddon.listeners;

import com.atherys.heroesaddon.HeroesAddon;
import com.herocraftonline.heroes.api.events.HeroJoinPartyEvent;
import com.herocraftonline.heroes.api.events.HeroLeavePartyEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.party.HeroParty;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.*;
import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.HashSet;
import java.util.Set;

import static org.bukkit.ChatColor.translateAlternateColorCodes;

public class HeroesPartyListener
        implements Listener {
    private static HeroesAddon heroesaddon;
    private ScoreboardManager manager;
    private PermissionManager pex = PermissionsEx.getPermissionManager();

    Set<Scoreboard> boards = new HashSet<>();

    public HeroesPartyListener(HeroesAddon heroesaddon) {
        HeroesPartyListener.heroesaddon = heroesaddon;
        this.manager = Bukkit.getScoreboardManager();

        Scoreboard board = this.manager.getMainScoreboard();

        this.boards.add(board);

        if (board.getObjective("HP") == null) {
            Objective objective = board.registerNewObjective("HP", "health");
            objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
            objective.setDisplayName(ChatColor.RED + "Health");
        }

        if (board.getTeam("mod") == null) {
            Team team = board.registerNewTeam("mod");
            team.setAllowFriendlyFire(true);
            team.setPrefix(ChatColor.DARK_GREEN + "");
        }

        if (board.getTeam("admin") == null) {
            Team team = board.registerNewTeam("admin");
            team.setAllowFriendlyFire(true);
            team.setPrefix(ChatColor.DARK_RED + "");
        }
    }

    public String getPartyColor() {
        String color = heroesaddon.getConfig().getString("party-color");
        return translateAlternateColorCodes('&', "&" + color);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        Hero hero = HeroesAddon.getHeroes().getCharacterManager().getHero(player);
        if ((!hero.hasParty()) || (!hero.getParty().getLeader().equals(hero)))
            return;
        HeroParty party = hero.getParty();

        if (event.getMessage().toLowerCase().startsWith("/party kick ")) {
            String[] args = event.getMessage().replace("/party kick ", "").split(" ");
            if (args.length != 1) {
                return;
            }

            Player tplayer = Bukkit.getPlayer(args[0]);
            if (tplayer == null) {
                return;
            }

            Hero target = HeroesAddon.getHeroes().getCharacterManager().getHero(tplayer);
            if (!party.isPartyMember(target)) {
                return;
            }
            HeroLeavePartyEvent LeavePartyEvent = new HeroLeavePartyEvent(target, party, HeroLeavePartyEvent.LeavePartyReason.KICK);
            Bukkit.getServer().getPluginManager().callEvent(LeavePartyEvent);
            if (LeavePartyEvent.isCancelled())
                return;
            party.messageParty(tplayer.getName() + " was kicked from the party!");
            party.removeMember(target);
            event.setCancelled(true);
        }

        if (event.getMessage().toLowerCase().startsWith("/party leader ")) {
            String[] args = event.getMessage().replace("/party leader ", "").split(" ");
            if (args.length != 1) {
                return;
            }

            Player tplayer = Bukkit.getPlayer(args[0]);
            if (tplayer == null) {
                return;
            }

            Hero target = HeroesAddon.getHeroes().getCharacterManager().getHero(tplayer);
            if (!party.isPartyMember(target)) {
                return;
            }
            party.messageParty(tplayer.getName() + " is now the leader of the party!");
            party.setLeader(target);
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPartyJoin(HeroJoinPartyEvent event) {
        HeroParty party = event.getParty();
        int index = party.toString().lastIndexOf("@");
        Player pleader = party.getLeader().getPlayer();
        Hero hero = event.getHero();
        Scoreboard board = pleader.getScoreboard();
        if (board == this.manager.getMainScoreboard()) {
            board = this.manager.getNewScoreboard();
            Objective objective = board.registerNewObjective("HP", "health");
            objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
            objective.setDisplayName(ChatColor.RED + "Health");

            for (Hero phero : party.getMembers()) {
                objective.getScore(phero.getPlayer().getName()).setScore((int) phero.getPlayer().getHealth());
            }

            this.boards.add(board);

            if (board.getTeam("mod") == null) {
                Team team = board.registerNewTeam("mod");
                team.setAllowFriendlyFire(true);
                team.setPrefix(ChatColor.DARK_GREEN + "");
            }
            if (board.getTeam("admin") == null) {
                Team team = board.registerNewTeam("admin");
                team.setAllowFriendlyFire(true);
                team.setPrefix(ChatColor.DARK_RED + "");
            }

            for (Player player : Bukkit.getOnlinePlayers()) {
                if (!party.isPartyMember(player)) {
                    setPlayerColour(player, board);
                }
            }
        }

        Team team = board.getTeam(party.toString().substring(index));

        if (team == null) {
            team = board.registerNewTeam(party.toString().substring(index));
            team.setPrefix(getPartyColor());
            team.addEntry(pleader.getName());
        }

        pleader.setScoreboard(board);

        team.addEntry(hero.getPlayer().getName());
        hero.getPlayer().setScoreboard(board);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPartyLeave(HeroLeavePartyEvent event) {
        HeroParty party = event.getParty();
        int index = party.toString().lastIndexOf("@");
        if (party.getMembers().size() > 1) {
            Player pleader = party.getLeader().getPlayer();
            Scoreboard board = pleader.getScoreboard();
            Team team = board.getTeam(party.toString().substring(index));
            if (team != null) {
                team.removeEntry(event.getHero().getPlayer().getName());
                if (party.getMembers().size() == 2) {
                    team.unregister();
                    for (Hero he : party.getMembers()) {
                        he.getPlayer().setScoreboard(this.manager.getMainScoreboard());
                    }
                    this.boards.remove(board);
                } else {
                    setPlayerColour(event.getHero().getPlayer(), board);
                    event.getHero().getPlayer().setScoreboard(this.manager.getMainScoreboard());
                }
            }
        }
    }


    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        for (Scoreboard board : boards) {
            setPlayerColour(player, board);
            Objective objective = board.getObjective("HP");
            objective.getScore(player.getName()).setScore((int) player.getHealth());
        }
    }

    public void setPlayerColour(Player player, Scoreboard board) {
        if (board.getTeam(player.getName()) != null)
            return;
        PermissionUser user = this.pex.getUser(player);
        if (user.inGroup("Magistrate") || user.inGroup("Owner"))
            board.getTeam("admin").addEntry(player.getName());
        else if (user.inGroup("Justicar"))
            board.getTeam("mod").addEntry(player.getName());
    }
}