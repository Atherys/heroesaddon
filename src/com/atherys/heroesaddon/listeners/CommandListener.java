package com.atherys.heroesaddon.listeners;
//Can remove

import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.lib.Lib;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.classes.HeroClass;
import com.herocraftonline.heroes.characters.skill.*;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.HashMap;
import java.util.Map;

public class CommandListener implements Listener {

    private static final int SKILLS_PER_PAGE = 6;

    private final HeroesAddon heroesAddon;

    public CommandListener(HeroesAddon heroesAddon) {
        this.heroesAddon = heroesAddon;
    }

    public void broadcastmessage(Player player) {
        for (Entity e : player.getNearbyEntities(30, 30, 30)) {
            if (!(e instanceof Player)) {
                continue;
            }
            e.sendMessage("[" + ChatColor.RED + "Teleport" + ChatColor.WHITE + "] " + player.getDisplayName() + " is teleporting away at " + (int) player.getLocation().getX() + "X, " + (int) player.getLocation().getY() + "Y, " + (int) player.getLocation().getZ() + "Z.");
        }
    }

    private void skillListCommand(Player player, String[] args) {
        Hero hero = heroesAddon.getHeroes().getCharacterManager().getHero(player);
        HeroClass heroClass = hero.getHeroClass();
        HeroClass secondClass = hero.getSecondClass();
        int page = 0;
        boolean prim = true;
        boolean sec = true;
        HeroClass hc = null;
        HeroClass titleClass = hero.getHeroClass();
        if (args.length != 0) {
            try {
                page = Integer.parseInt(args[0]) - 1;
            }
            catch (NumberFormatException e) {
                if (args[0].toLowerCase().contains("prim")) {
                    sec = false;
                    titleClass = heroClass;
                }
                else if (args[0].toLowerCase().contains("pro")) {
                    prim = false;
                    if (secondClass == null) {
                        Messaging.send(player, "You don't have a secondary class!", new Object[0]);
                        return;
                    }
                    titleClass = secondClass;
                }
                else {
                    hc = heroesAddon.getHeroes().getClassManager().getClass(args[0]);
                    if (hc == null) {
                        Messaging.send(player, "That class does not exist!", new Object[0]);
                        return;
                    }
                    prim = false;
                    sec = false;
                    titleClass = hc;
                }
                if (args.length > 1) {
                    try {
                        page = Integer.parseInt(args[1]) - 1;
                    }
                    catch (NumberFormatException ex) {}
                }
            }
        }
        final Map<SkillListInfo, Integer> skills = new HashMap<SkillListInfo, Integer>();
        if (prim) {
            for (final String name : heroClass.getSkillNames()) {
                final Skill skill = heroesAddon.getHeroes().getSkillManager().getSkill(name);
                final int level = SkillConfigManager.getSetting(heroClass, skill, SkillSetting.LEVEL.node(), 1);
                skills.put(new SkillListInfo(heroClass, skill), level);
            }
        }
        if (sec && secondClass != null) {
            for (final String name : secondClass.getSkillNames()) {
                final Skill skill = heroesAddon.getHeroes().getSkillManager().getSkill(name);
                final int level = SkillConfigManager.getSetting(secondClass, skill, SkillSetting.LEVEL.node(), 1);
                skills.put(new SkillListInfo(secondClass, skill), level);
            }
        }
        if (hc != null) {
            for (final String name : hc.getSkillNames()) {
                final Skill skill = heroesAddon.getHeroes().getSkillManager().getSkill(name);
                final int level = SkillConfigManager.getSetting(hc, skill, SkillSetting.LEVEL.node(), 1);
                skills.put(new SkillListInfo(hc, skill), level);
            }
        }
        int numPages = skills.size() / SKILLS_PER_PAGE;
        if (skills.size() % SKILLS_PER_PAGE != 0) {
            ++numPages;
        }
        if (page >= numPages || page < 0) {
            page = 0;
        }
        player.sendMessage(ChatColor.RED + "<--- " + Lib.getClassColor(titleClass) + ChatColor.BOLD + titleClass.getName() + ChatColor.RESET + ChatColor.WHITE + " Skills <" + (page + 1) + "/" + numPages + ">" + ChatColor.RED + " --->");
        final int start = page * SKILLS_PER_PAGE;
        int end = start + SKILLS_PER_PAGE;
        if (end > skills.size()) {
            end = skills.size();
        }
        int count = 0;
        for (final Map.Entry<SkillListInfo, Integer> entry : SkillListInfo.entriesSortedByValues(skills)) {
            if (count >= start && count < end) {
                final SkillListInfo sli = entry.getKey();
                final int level2 = entry.getValue();
                hc = sli.getHeroClass();
                ChatColor color;
                if (level2 > hero.getLevel(hc)) {
                    color = ChatColor.RED;
                }
                else {
                    color = ChatColor.GREEN;
                }
                StringBuilder stringBuilder = new StringBuilder();
                String description = Lib.getSkillDescription(hc, sli.getSkill());
                if (description == null || description.isEmpty()) {
                    description = sli.getSkill().getDescription(hero);
                }
                stringBuilder.append("  ").append(color).append(" ").append(hc.getName().substring(0, (3 > hc.getName().length()) ? hc.getName().length() : 3))
                        .append(" ").append(level2).append(" ").append(ChatColor.YELLOW).append(ChatColor.BOLD).append(sli.getSkill().getName())
                        .append(ChatColor.RESET).append(ChatColor.YELLOW).append(" (").append(sli.getSkill() instanceof PassiveSkill ? "Passive" : "Active").append("): ")
                        .append(ChatColor.GOLD).append(description);
                player.sendMessage(stringBuilder.toString());
            }
            ++count;
        }
        player.sendMessage(ChatColor.RED + "<---" + ChatColor.YELLOW + " To use a skill, type " + ChatColor.WHITE +
                "/skill <name>" + ChatColor.YELLOW + "." + ((page + 2) > numPages ? "" :
                " To go to the next Skills-page, type " + ChatColor.WHITE +
                "/skills " + (titleClass.getName().equalsIgnoreCase(hero.getHeroClass().getName()) ? "" : titleClass.getName() + " ")
                        + (page + 2) + ChatColor.YELLOW + ".") + ChatColor.RED + " --->");
    }

    @EventHandler (priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onSkillsCommand(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().toLowerCase().contains("/skills")) {
            Player player = event.getPlayer();
            String[] args = event.getMessage().toLowerCase().replace("/skills", "").trim().split(" +");
            if (args.length == 1 && args[0].isEmpty()) {
                args = new String[0];
            }
            skillListCommand(player, args);
            event.setCancelled(true);
        } else if (event.getMessage().toLowerCase().contains("/hero skills")) {
            Player player = event.getPlayer();
            String[] args = event.getMessage().toLowerCase().replace("/hero skills", "").trim().split(" +");
            if (args.length == 1 && args[0].isEmpty()) {
                args = new String[0];
            }
            skillListCommand(player, args);
            event.setCancelled(true);
        } else if (event.getMessage().toLowerCase().contains("/spawn")) {
            Hero hero = heroesAddon.getHeroes().getCharacterManager().getHero(event.getPlayer());
            if (hero.isInCombat() && !event.getPlayer().isOp()) {
                Messaging.send(hero.getPlayer(), "You can't use that command in combat!");
                event.setCancelled(true);
            }
        }
    }
}
