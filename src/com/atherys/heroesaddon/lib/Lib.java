package com.atherys.heroesaddon.lib;

import com.atherys.heroesaddon.HeroesAddon;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.classes.HeroClass;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.*;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.util.Util;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.material.Door;
import org.bukkit.util.BlockIterator;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.logging.Level;


public class Lib {
    public static final Map<String, Enchantment> enchants;
    public static Set<Material> foodTypes = EnumSet.noneOf(Material.class);
    public static Set<Material> enchantableItems = EnumSet.noneOf(Material.class);
    public static List<double[]> playerParticleCoords = new ArrayList<>();
    private static Heroes heroes = (Heroes) Bukkit.getServer().getPluginManager().getPlugin("Heroes");

    static {
        foodTypes.add(Material.BREAD);
        foodTypes.add(Material.COOKIE);
        foodTypes.add(Material.PORK);
        foodTypes.add(Material.COOKED_BEEF);
        foodTypes.add(Material.COOKED_FISH);
        foodTypes.add(Material.APPLE);
    }

    static {
        enchantableItems.add(Material.DIAMOND_AXE);
        enchantableItems.add(Material.DIAMOND_SWORD);
        enchantableItems.add(Material.DIAMOND_AXE);
        enchantableItems.add(Material.DIAMOND_SWORD);
        enchantableItems.add(Material.DIAMOND_PICKAXE);
        enchantableItems.add(Material.DIAMOND_SPADE);
        enchantableItems.add(Material.DIAMOND_HOE);
        enchantableItems.add(Material.DIAMOND_HELMET);
        enchantableItems.add(Material.DIAMOND_CHESTPLATE);
        enchantableItems.add(Material.DIAMOND_LEGGINGS);
        enchantableItems.add(Material.DIAMOND_BOOTS);
        enchantableItems.add(Material.BOW);
        enchantableItems.add(Material.FISHING_ROD);
        enchantableItems.add(Material.SHEARS);
        enchantableItems.add(Material.IRON_AXE);
        enchantableItems.add(Material.IRON_SWORD);
        enchantableItems.add(Material.IRON_PICKAXE);
        enchantableItems.add(Material.IRON_SPADE);
        enchantableItems.add(Material.IRON_HOE);
        enchantableItems.add(Material.IRON_HELMET);
        enchantableItems.add(Material.IRON_CHESTPLATE);
        enchantableItems.add(Material.IRON_LEGGINGS);
        enchantableItems.add(Material.IRON_BOOTS);
        enchantableItems.add(Material.GOLD_AXE);
        enchantableItems.add(Material.GOLD_SWORD);
        enchantableItems.add(Material.GOLD_PICKAXE);
        enchantableItems.add(Material.GOLD_SPADE);
        enchantableItems.add(Material.GOLD_HOE);
        enchantableItems.add(Material.GOLD_HELMET);
        enchantableItems.add(Material.GOLD_CHESTPLATE);
        enchantableItems.add(Material.GOLD_LEGGINGS);
        enchantableItems.add(Material.GOLD_BOOTS);
        enchantableItems.add(Material.CHAINMAIL_HELMET);
        enchantableItems.add(Material.CHAINMAIL_CHESTPLATE);
        enchantableItems.add(Material.CHAINMAIL_LEGGINGS);
        enchantableItems.add(Material.CHAINMAIL_BOOTS);
        enchantableItems.add(Material.LEATHER_HELMET);
        enchantableItems.add(Material.LEATHER_CHESTPLATE);
        enchantableItems.add(Material.LEATHER_LEGGINGS);
        enchantableItems.add(Material.LEATHER_BOOTS);
        enchantableItems.add(Material.STONE_AXE);
        enchantableItems.add(Material.STONE_SWORD);
        enchantableItems.add(Material.STONE_PICKAXE);
        enchantableItems.add(Material.STONE_SPADE);
        enchantableItems.add(Material.STONE_HOE);
        enchantableItems.add(Material.WOOD_AXE);
        enchantableItems.add(Material.WOOD_SWORD);
        enchantableItems.add(Material.WOOD_PICKAXE);
        enchantableItems.add(Material.WOOD_SPADE);
        enchantableItems.add(Material.WOOD_HOE);
    }

    static {
        Map<String, Enchantment> map = new HashMap<String, Enchantment>();
        map.put("Feather Falling", Enchantment.PROTECTION_FALL);
        map.put("Respiration", Enchantment.OXYGEN);
        map.put("Aqua Affinity", Enchantment.WATER_WORKER);
        map.put("Thorns", Enchantment.THORNS);
        map.put("Unbreaking", Enchantment.DURABILITY);
        map.put("Knockback", Enchantment.KNOCKBACK);
        map.put("Fire Aspect", Enchantment.FIRE_ASPECT);
        map.put("Flame", Enchantment.ARROW_FIRE);
        map.put("Infinity", Enchantment.ARROW_INFINITE);
        map.put("Efficiency", Enchantment.DIG_SPEED);
        map.put("Luck", Enchantment.LUCK);
        map.put("Lure", Enchantment.LURE);
        enchants = Collections.unmodifiableMap(map);
    }

    static {
        playerParticleCoords = new ArrayList<>();
        double t = 0;
        double r = 0.5;
        for (int i = 0; i < 8; i++) {
            t += Math.PI/4;
            double x = r * Math.cos(t);
            double y = 0.2;
            double z = r * Math.sin(t);
            playerParticleCoords.add(new double[] { x, y, z });
        }
        r = 0.7;
        t = 0;
        for (int i = 0; i < 8; i++) {
            t += Math.PI/4;
            double x = r * Math.cos(t);
            double y = 1;
            double z = r * Math.sin(t);
            playerParticleCoords.add(new double[] { x, y, z });
        }
        t = 0;
        r = 0.5;
        for (int i = 0; i < 8; i++) {
            t += Math.PI/4;
            double x = r * Math.cos(t);
            double y = 1.8;
            double z = r * Math.sin(t);
            playerParticleCoords.add(new double[] { x, y, z });
        }
    }

    public static void createExplosion(Location loc, float size, Player p) {
        createExplosion(loc.getX(), loc.getBlockY(), loc.getZ(), size, p);
    }

    public static void createExplosion(double x, double y, double z, float size, Player p) {
        ProtocolManager prManager = ProtocolLibrary.getProtocolManager();
        PacketContainer fexplosion = prManager.createPacket(PacketType.Play.Server.EXPLOSION);
        fexplosion.getDoubles().write(0, x).write(1, y).write(2, z);
        fexplosion.getFloat().write(0, size);
        try {
            prManager.sendServerPacket(p, fexplosion);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Heroes Error- Unable to send packet: " + fexplosion, e);
        }

    }

    public static void createExplosionForNearby(Location loc, int radius, float size) {
        loc.getWorld().getNearbyEntities(loc, radius, radius, radius).stream().filter(entity -> entity instanceof Player).forEach(entity -> createExplosion(loc, size, (Player) entity));
    }

    public static void createSoundNearby(Location location, double radius, Sound sound, float pitch, float volume) {
        location.getWorld().getNearbyEntities(location, radius, radius, radius).stream().filter(entity -> entity instanceof Player).forEach(entity -> {
            Player player = (Player) entity;
            player.playSound(location, sound, pitch, volume);
        });
    }

    public static List<Entity> getNearbyEntites(Location loc, int radius) {
        List<Entity> list = new ArrayList<>();
        Snowball s = (Snowball) loc.getWorld().spawnEntity(loc, EntityType.SNOWBALL);
        for (Entity e : s.getNearbyEntities(radius, radius, radius)) {
            list.add(e);
        }
        s.remove();
        return list;
    }

    public static void openDoor(Block block) {
        if (block.getType() == Material.IRON_DOOR_BLOCK) {
            Door door = (Door) block.getState().getData();
            byte data = block.getData();
            if (door.isOpen()) {
                data = (byte) (data & 0xB);
                block.setData(data, true);
            } else {
                data = (byte) (data | 0x4);
                block.setData(data, true);
            }
        } else if ((block.getType() == Material.WOODEN_DOOR) || (block.getType() == Material.TRAP_DOOR)) {
            Door door = (Door) block.getState().getData();
            byte data = block.getData();
            if (!door.isOpen()) {
                data = (byte) (data ^ 0x4);
                block.setData(data, true);
            }
        }
    }

    public static Entity[] getNearbyEntities(Location l, int radius) {
        int chunkRadius = radius < 16 ? 1 : (radius - (radius % 16)) / 16;
        HashSet<Entity> radiusEntities = new HashSet<>();
        for (int chX = 0 - chunkRadius; chX <= chunkRadius; chX++) {
            for (int chZ = 0 - chunkRadius; chZ <= chunkRadius; chZ++) {
                int x = (int) l.getX(), y = (int) l.getY(), z = (int) l.getZ();
                for (Entity e : new Location(l.getWorld(), x + (chX * 16), y, z + (chZ * 16)).getChunk().getEntities()) {
                    if (e.getLocation().distanceSquared(l) <= radius * radius && e.getLocation().getBlock() != l.getBlock()) {
                        radiusEntities.add(e);
                    }
                }
            }
        }
        return radiusEntities.toArray(new Entity[radiusEntities.size()]);
    }

    public static boolean healHero(Hero hero, double amount, Skill skill, Hero healer) {
        HeroRegainHealthEvent hrhEvent = new HeroRegainHealthEvent(hero, amount, skill, healer);
        Heroes.getInstance().getServer().getPluginManager().callEvent(hrhEvent);
        if (!hrhEvent.isCancelled()) {
            hero.heal(hrhEvent.getDelta());
            return true;
        }
        return false;
    }

    public static ChatColor getClassColor(HeroClass heroClass) {
        if (heroClass == null) {
            return ChatColor.GRAY;
        }
        String className = heroClass.hasNoParents() ? heroClass.getName() : heroClass.getParents().get(0).getName();
        switch (className) {
            case "Squire":
                return ChatColor.RED;
            case "Knave":
                return ChatColor.YELLOW;
            case "Initiate":
                return ChatColor.BLUE;
            case "Acolyte":
                return ChatColor.GREEN;
            default:
                return ChatColor.GRAY;
        }
    }

    public static String getEffectClass(Effect effect) {
        if (effect instanceof ExpirableEffect) {
            return "ExpirableEffect" + ChatColor.GRAY + ", Time Remaining: " + ChatColor.WHITE + ((ExpirableEffect) effect).getRemainingTime() / 1000;
        } else if (effect instanceof PeriodicDamageEffect) {
            return "PeriodicDamageEffect" + ChatColor.GRAY + ", Time Remaining: " + ChatColor.WHITE + ((PeriodicDamageEffect) effect).getRemainingTime() / 1000;
        } else if (effect instanceof PeriodicHealEffect) {
            return "PeriodicHealEffect" + ChatColor.GRAY + ", Time Remaining: " + ChatColor.WHITE + ((PeriodicHealEffect) effect).getRemainingTime() / 1000;
        } else if (effect instanceof PeriodicExpirableEffect) {
            return "PeriodicExpirableEffect" + ChatColor.GRAY + ", Time Remaining: " + ChatColor.WHITE + ((PeriodicExpirableEffect) effect).getRemainingTime() / 1000;
        } else if (effect instanceof PeriodicEffect) {
            return "PeriodicEffect";
        }
        return "Effect";
    }

    public static String getSkillCostStats(Hero hero, Skill skill) {

        //String description = "";
        StringBuilder sb = new StringBuilder();
        boolean costs = false;
        sb.append(" ");

        // MANA
        int mana = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.MANA.node(), 0, false);
        if (mana > 0) {
            sb.append("Mana: ").append(mana).append(" ");
            costs = true;
        }

        // COOLDOWN
        int cooldown = (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.COOLDOWN.node(), 0, false) / 1000);
        if (cooldown > 0) {
            sb.append("CD: ").append(cooldown).append("s ");
            costs = true;
        }

        // HEALTH_COST
        int healthCost = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.HEALTH_COST.node(), 0, false);
        if (healthCost > 0) {
            sb.append("HP: ").append(healthCost).append(" ");
            costs = true;
        }

        // DELAY
        int delay = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DELAY.node(), 0, false) / 1000;
        if (delay > 0) {
            sb.append("W: ").append(delay).append("s ");
            costs = true;
        }
        if (costs)
            return sb.toString();
        else
            return "";
    }

    public static String getSkillCostStats(HeroClass heroClass, Skill skill) {

        //String description = "";
        StringBuilder sb = new StringBuilder();
        boolean costs = false;
        sb.append(" ");

        if (skill.getName().equalsIgnoreCase("Defences")) {
            double magicResistance = SkillConfigManager.getSetting(heroClass, skill, "magic-damage-reduction", 0.0);
            magicResistance *= 100;
            if (magicResistance > 0) {
                sb.append("Magic damage reduction: ").append(ChatColor.WHITE).append((int) magicResistance).append("%").append(ChatColor.GOLD).append(" ");
                costs = true;
            }

            double fallReduction = SkillConfigManager.getSetting(heroClass, skill, "falling-damage-reduction", 0.0);
            fallReduction *= 100;
            if (fallReduction > 0) {
                sb.append("Fall damage reduction: ").append(ChatColor.WHITE).append((int) fallReduction).append("%").append(ChatColor.GOLD).append(" ");
                costs = true;
            }

            return costs ? sb.toString() : "";
        }

        // MANA
        int mana = SkillConfigManager.getSetting(heroClass, skill, SkillSetting.MANA.node(), 0);
        if (mana > 0) {
            sb.append("Mana: ").append(ChatColor.WHITE).append(mana).append(ChatColor.GOLD).append(" ");
            costs = true;
        }

        // COOLDOWN
        double cooldown = (SkillConfigManager.getSetting(heroClass, skill, SkillSetting.COOLDOWN.node(), 0) / 1000);
        if (cooldown > 0) {
            sb.append("CD: ").append(ChatColor.WHITE);
            if (cooldown % 1 == 0) {
                sb.append((int) cooldown);
            } else {
                sb.append(cooldown);
            }
            sb.append(ChatColor.GOLD).append("s ");
            costs = true;
        }

        // HEALTH_COST
        double healthCost = SkillConfigManager.getSetting(heroClass, skill, SkillSetting.HEALTH_COST.node(), 0);
        if (healthCost > 0) {
            sb.append("HP: ").append(ChatColor.WHITE);
            if (healthCost % 1 == 0) {
                sb.append((int) healthCost);
            } else {
                sb.append(healthCost);
            }
            sb.append(ChatColor.GOLD).append("s ");
            costs = true;
        }

        // DELAY
        double delay = SkillConfigManager.getSetting(heroClass, skill, SkillSetting.DELAY.node(), 0) / 1000;
        if (delay > 0) {
            sb.append("W: ").append(ChatColor.WHITE);
            if (delay % 1 == 0) {
                sb.append((int) delay);
            } else {
                sb.append(delay);
            }
            costs = true;
        }
        return costs ? sb.toString() : "";
    }

    public static List<Entity> getEntitiesInCone(List<Entity> entities, org.bukkit.util.Vector startPos, float radius, float degrees, org.bukkit.util.Vector direction) {
        List<Entity> newEntities = new ArrayList<Entity>(); // Returned list
        float squaredRadius = radius * radius; // We don't want to use square root
        for (Entity e : entities) {
            org.bukkit.util.Vector relativePosition = e.getLocation().toVector();
            // Position of the entity relative to the cone origin
            relativePosition.subtract(startPos);
            if (relativePosition.lengthSquared() > squaredRadius) {
                continue;
            } // First check : distance
            if (Math.abs((float) Math.toDegrees(direction.angle(relativePosition))) > degrees) {
                continue;
            } // Second check : angle
            newEntities.add(e); // The entity e is in the cone
        }
        return newEntities;
    }

    public static void partyCooldown(Skill skill, String name, Hero hero, long cooldown, int radius) {
        if (hero.hasParty()) {
            Player player = hero.getPlayer();
            long time = System.currentTimeMillis();
            for (Hero pHero : hero.getParty().getMembers()) {
                if (pHero.equals(hero)) {
                    continue;
                }
                Player pPlayer = pHero.getPlayer();
                if (radius == 0 || (pPlayer.getWorld().equals(player.getWorld()) && pPlayer.getLocation().distanceSquared(player.getLocation()) <= radius * radius)) {
                    if (pHero.hasAccessToSkill(skill) && (hero.getCooldown(name) == null || hero.getCooldown(name) < cooldown + time)) {
                        pHero.setCooldown(name, cooldown + time);
                    }
                }
            }
        }
    }

    public static void partyCooldown(Skill skill, String name, Hero hero, long cooldown) {
        partyCooldown(skill, name, hero, cooldown, 0);
    }

    public static void cancelDelayedSkill(Hero hero) {
        hero.cancelDelayedSkill();
        if (hero.hasEffect("SkillCasting")) {
            hero.removeEffect(hero.getEffect("SkillCasting"));
        }
    }

    public static void removeBlocks(Set<Block> blocks) {
        Set<Chunk> chunks = new HashSet<>();
        for (Block block : blocks) {
            Chunk chunk = block.getChunk();
            if (!chunks.contains(chunk) && !chunk.isLoaded()) {
                chunk.load();
                chunks.add(chunk);
            }
        }

        for (Block block : blocks) {
            if (block.hasMetadata("HeroesUnbreakableBlock")) {
                block.removeMetadata("HeroesUnbreakableBlock", HeroesAddon.getHeroes());
            }
            if (block.getType() != Material.AIR) {
                block.setType(Material.AIR);
            }
        }

        //Error checking
        for (Block block : blocks) {
            if (block.getType() != Material.AIR) {
                Bukkit.getServer().getLogger().log(Level.SEVERE, "Error removing block " + block.getType().name() + " in chunk " + block.getChunk().getX() + ", " + block.getChunk().getZ());
            }
        }

        for (Chunk chunk : chunks) {
            if (chunk.isLoaded()) {
                chunk.unload();
            } else {
                Bukkit.getServer().getLogger().log(Level.SEVERE, "Error: Chunk was never loaded, could not remove blocks!");
            }
        }
    }

    public static Block getTargetBlock(Player player, int distance) {
        Block previous = null;
        BlockIterator iterator;
        try {
            iterator = new BlockIterator(player, distance);
        } catch (IllegalStateException e) {
            return null;
        }
        while (iterator.hasNext()) {
            Block block = iterator.next();
            if (Util.transparentBlocks.contains(block.getType()) && (Util.transparentBlocks.contains(block.getRelative(BlockFace.UP).getType()) || Util.transparentBlocks.contains(block.getRelative(BlockFace.DOWN).getType()))) {
                previous = block;
            } else {
                break;
            }
        }
        return previous;
    }

    public static void projectileHit(Player player) {
        player.playSound(player.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 1);
    }

    public static String getSkillDescription(HeroClass heroClass, Skill skill) {
        Object object = SkillConfigManager.getSetting(heroClass, skill, "description");
        if (object == null || object.toString().isEmpty()) {
            return skill.getDescription() + Lib.getSkillCostStats(heroClass, skill);
        }
        String description = object.toString();
        for (int i = 0; i < 10 && description.contains("%"); i++) {
            String value = description.substring(description.indexOf('%'), description.indexOf('%', description.indexOf('%') + 1) + 1);
            String newValue = value.replace("%", "");
            Object nodeValue;
            boolean divide = false;
            boolean multiply = false;
            if (value.contains("/1000")) {
                newValue = newValue.substring(0, value.indexOf("/1000") - 1);
                divide = true;
                nodeValue = SkillConfigManager.getSetting(heroClass, skill, newValue);
            } else if (value.contains("*100")) {
                newValue = newValue.substring(0, value.indexOf("*100") - 1);
                multiply = true;
                nodeValue = SkillConfigManager.getSetting(heroClass, skill, newValue);
            } else {
                nodeValue = SkillConfigManager.getSetting(heroClass, skill, newValue);
            }
            if (nodeValue == null) {
                Bukkit.getLogger().log(Level.SEVERE, heroClass.getName() + " does not have " + newValue + " for skill " + skill.getName());
                return description;
            }
            if (divide) {
                double d = (Long.valueOf(nodeValue.toString()).doubleValue() / 1000);
                if (d % 1 == 0) {
                    newValue = "" + (int)d;
                } else {
                    newValue = "" + d;
                }
            } else if (multiply) {
                double d = (Double.valueOf(nodeValue.toString()) * 100);
                if (d % 1 == 0) {
                    newValue = "" + (int)d;
                } else {
                    newValue = "" + d;
                }
            } else {
                newValue = nodeValue.toString();
            }
            description = description.replace(value, ChatColor.WHITE + newValue + ChatColor.GOLD);
        }
        return description + Lib.getSkillCostStats(heroClass, skill);
    }

    public static boolean isBadEffect(Effect effect) {
        return ((!effect.isType(EffectType.BENEFICIAL) || !effect.isType(EffectType.HEALING) || !effect.isType(EffectType.IMBUE) ||
                !effect.isType(EffectType.INVIS) || !effect.isType(EffectType.INVISIBILITY) || !effect.isType(EffectType.INVULNERABILITY) ||
                !effect.isType(EffectType.SAFEFALL) || !effect.isType(EffectType.SPEED) || !effect.isType(EffectType.WATER_BREATHING))
                && (effect.isType(EffectType.BLEED) || effect.isType(EffectType.BLIND) || effect.isType(EffectType.CONFUSION) ||
                effect.isType(EffectType.DARK) || effect.isType(EffectType.DISABLE) || effect.isType(EffectType.DISARM) ||
                effect.isType(EffectType.DISEASE) ||  effect.isType(EffectType.FIRE) ||
                effect.isType(EffectType.HARMFUL) || effect.isType(EffectType.HUNGER) || effect.isType(EffectType.NAUSEA) ||
                effect.isType(EffectType.POISON) || effect.isType(EffectType.ROOT) || effect.isType(EffectType.SILENCE) ||
                effect.isType(EffectType.SLOW) || effect.isType(EffectType.STUN) || effect.isType(EffectType.WEAKNESS) ||
                effect.isType(EffectType.WITHER) ));
    }
}
