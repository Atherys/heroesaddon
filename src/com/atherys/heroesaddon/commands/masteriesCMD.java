package com.atherys.heroesaddon.commands;

import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.lib.Lib;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.classes.HeroClass;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class masteriesCMD extends CommandModule {
	public masteriesCMD() {
		//It defines the label, min args, and max args.
		super("masteries", 0, 1);
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command cannot be executed from console");
			return;
		}
		Player psender = (Player) sender;
		if (args.length == 0) {
			psender.sendMessage(printMasterClasses(psender));
			return;
		}
		if (args.length == 1) {
			Player player = Bukkit.getPlayer(args[0]);
			if (player == null) {
				psender.sendMessage("Offline players are not supported.");
				return;
			}
			psender.sendMessage(printMasterClasses(player));
			return;
		}
		//psender.sendMessage("Invalid arguments.");
		return;
	}

	private String printMasterClasses(Player target) {
		Hero hero = HeroesAddon.getHeroes().getCharacterManager().getHero(target);

		StringBuilder sb = new StringBuilder();
		sb.append(ChatColor.GRAY).append(target.getName()).append(" is master ");
		boolean mastered = false;

		List<HeroClass> mages = new ArrayList<>();
		List<HeroClass> rogues = new ArrayList<>();
		List<HeroClass> warriors = new ArrayList<>();
		List<HeroClass> acolytes = new ArrayList<>();
		for (HeroClass heroClass : HeroesAddon.getHeroes().getClassManager().getClasses()) {
			if (!hero.isMaster(heroClass)) continue;
			mastered = true;
			if (heroClass.getName().equalsIgnoreCase("Initiate") || (heroClass.getParents().size() >= 1 && heroClass.getParents().get(0).getName().equalsIgnoreCase("Initiate"))) {
				mages.add(heroClass);
				continue;
			} else if (heroClass.getName().equalsIgnoreCase("Knave") || (heroClass.getParents().size() >= 1 && heroClass.getParents().get(0).getName().equalsIgnoreCase("Knave"))) {
				rogues.add(heroClass);
				continue;
			} else if (heroClass.getName().equalsIgnoreCase("Squire") || (heroClass.getParents().size() >= 1 && heroClass.getParents().get(0).getName().equalsIgnoreCase("Squire"))) {
				warriors.add(heroClass);
				continue;
			} else {
				acolytes.add(heroClass);
				continue;
			}

	}

		if (!mastered) {
			return target.getName() + ChatColor.GRAY + " is not a master of any class.";
		}

		for (HeroClass heroClass : mages) {
			sb.append(Lib.getClassColor(heroClass)).append(heroClass.getName()).append(ChatColor.GRAY).append(", ");
		}
		for (HeroClass heroClass : rogues) {
			sb.append(Lib.getClassColor(heroClass)).append(heroClass.getName()).append(ChatColor.GRAY).append(", ");
		}
		for (HeroClass heroClass : warriors) {
			sb.append(Lib.getClassColor(heroClass)).append(heroClass.getName()).append(ChatColor.GRAY).append(", ");
		}
		for (HeroClass heroClass : acolytes) {
			sb.append(Lib.getClassColor(heroClass)).append(heroClass.getName()).append(ChatColor.GRAY).append(", ");
		}
		return sb.toString();
	}
}
