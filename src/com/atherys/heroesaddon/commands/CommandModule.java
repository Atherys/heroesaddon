package com.atherys.heroesaddon.commands;
import com.atherys.heroesaddon.HeroesAddon;
import org.bukkit.command.CommandSender;

public abstract class CommandModule
{
	public String label;
	public int minArgs;
	public int maxArgs;

	/**
	 * @param label - The label of the command.
	 * @param minArgs - The minimum amount of arguments.
	 * @param maxArgs - The maximum amount of arguments.
	 */
	public CommandModule(String label, int minArgs, int maxArgs)
	{
		this.label = label;
		this.minArgs = minArgs;
		this.maxArgs = maxArgs;

		HeroesAddon.instance.getCommand(label).setExecutor(new CmdExecutor());
		HeroesAddon.commands.put(label, this);
	}

	//This method will process the command.
	public abstract void run(CommandSender sender, String[] args);
}