package com.atherys.heroesaddon.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventException;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.RegisteredListener;

import java.util.logging.Level;

public class herodebugCMD extends CommandModule {
	public herodebugCMD() {
		//It defines the label, min args, and max args.
		super("herodebug", 0, 1);
	}

	//The method that "runs" the command.
	public void run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command cannot be executed from console");
			return;
		}
		Player player = (Player) sender;
		if (!player.isOp()) {
			sender.sendMessage("You need to be opped to use this command!");
			return;
		}
		if (args.length < 1) {
			sender.sendMessage("Specify a player.");
			return;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			sender.sendMessage("Offline players are not supported.");
			return;
		}
		EntityDamageEvent event = new EntityDamageEvent(target, EntityDamageEvent.DamageCause.ENTITY_ATTACK, 10.0);
		for (RegisteredListener registeredListener : event.getHandlers().getRegisteredListeners()) {
			try {
				double damage = event.getDamage();
				registeredListener.callEvent(event);
				if (event.isCancelled()) {
					sender.sendMessage(ChatColor.RED + "EntityDamage cancelled by: " + ChatColor.GRAY + registeredListener.getListener());
				}
				if (event.getDamage() != damage) {
					sender.sendMessage(ChatColor.RED + "EntityDamage damage changed by: " + ChatColor.GRAY + registeredListener.getListener());
				}
			} catch (EventException e) {
				Bukkit.getLogger().log(Level.SEVERE, "Error caused by " + registeredListener.getListener() + " from herodebug command. (" + e.getCause() + ")");
			}
		}
		EntityDamageByEntityEvent event2 = new EntityDamageByEntityEvent(player, target, EntityDamageEvent.DamageCause.ENTITY_ATTACK, 10.0);
		for (RegisteredListener registeredListener : event2.getHandlers().getRegisteredListeners()) {
			try {
				double damage = event2.getDamage();
				registeredListener.callEvent(event2);
				if (event2.isCancelled()) {
					sender.sendMessage(ChatColor.RED + "EntityDamageByEntity cancelled by: " + ChatColor.GRAY + registeredListener.getListener());
				}
				if (event2.getDamage() != damage) {
					sender.sendMessage(ChatColor.RED + "EntityDamageByEntity damage changed by: " + ChatColor.GRAY + registeredListener.getListener());
				}
			} catch (EventException e) {
				Bukkit.getLogger().log(Level.SEVERE, "Error caused by " + registeredListener.getListener() + " from herodebug command. (" + e.getCause() + ")");
			}
		}
	}
}
