package com.atherys.heroesaddon.commands;

import com.atherys.heroesaddon.HeroesAddon;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CooldownResetSubCommand extends CommandModule {
	public CooldownResetSubCommand() {
		super("cdr", 0, 20);
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		if ((sender instanceof Player)) {
			if (args.length > 0) {
				List<Player> players = new ArrayList<>();
				for (String arg : args) {
					Player player = Bukkit.getPlayer(arg);
					if (player == null) {
						Messaging.send(sender, "No player named $1 was found!", args[0]);
					} else {
						players.add(player);
					}
				}
				if (players.isEmpty()) {
					return;
				}
				StringBuilder names = new StringBuilder();
				for (Player player : players) {
					Hero theLuckyHero = HeroesAddon.getHeroes().getCharacterManager().getHero(player);
					theLuckyHero.clearCooldowns();
					Messaging.send(player, "The gods have cleared your cooldowns!");
					names.append(player.getDisplayName()).append("  ");
				}
				Messaging.send(sender, "You have reset the cooldowns of: $1", names.toString());
				return;
			}
			if (!(sender instanceof Player)) {
				sender.sendMessage("Specify one or more players whose cooldowns to reset!");
				return;
			}
			Hero hero = HeroesAddon.getHeroes().getCharacterManager().getHero((Player) sender);
			hero.clearCooldowns();
			Messaging.send(sender, "You reset your cooldowns.");
			return;
		}
	}
}
