package com.atherys.heroesaddon.commands;

import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.lib.Lib;
import com.herocraftonline.heroes.characters.classes.HeroClass;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class classesWithCMD extends CommandModule {
	public classesWithCMD() {
		super("Classeswith", 0, 1);
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command cannot be executed from console");
			return;
		}
		if (args.length == 1) {
			boolean found = false;
			String skill = args[0];
			List<String> result = new ArrayList<>();
			result.add(ChatColor.GRAY + "Classes with the skill " + ChatColor.WHITE + skill + ":");
			for (HeroClass heroClass : HeroesAddon.getHeroes().getClassManager().getClasses()) {
				if (heroClass.hasSkill(skill)) {
					if (!heroClass.getName().equals("Admin") && !heroClass.getName().equals("Test")) {
						result.add(ChatColor.GRAY + "- " + Lib.getClassColor(heroClass) + heroClass.getName());
						found = true;
					}
				}
			}
			if (found) {
				for (String message : result) {
					sender.sendMessage(message);
				}
			} else {
				sender.sendMessage(ChatColor.GRAY + "No classes found with the skill " + ChatColor.WHITE + skill + ChatColor.GRAY + ".");
			}
			return;
		}
		sender.sendMessage("Invalid arguments.");
		return;
	}
}
