package com.atherys.heroesaddon.commands;

import ca.xshade.questionmanager.Option;
import ca.xshade.questionmanager.Question;
import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.lib.cancel911;
import com.atherys.heroesaddon.lib.run911;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class nineOneOneCMD extends CommandModule {
	public nineOneOneCMD() {
		super("911", 0, 0);
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command cannot be executed from console");
			return;
		}
		Player activator = (Player) sender;
		if (!HeroesAddon.getPex().has(activator, "heroesaddon.911")) {
			return;
		}
		List<Option> options = new ArrayList<>();
		HeroesAddon.getQuestioner().loadClasses();
		options.add(new Option("Fuck Yea!", new run911(activator)));
		options.add(new Option("Hell No!", new cancel911(activator)));
		Question question = new Question(activator.getName().toLowerCase(), "Are you sure you want to enact the 9/11 Protocol?", options);
		try {
			HeroesAddon.getQuestioner().appendQuestion(question);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
