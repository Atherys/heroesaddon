package com.atherys.heroesaddon.commands;

import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.lib.Lib;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class heroEffectsCMD extends CommandModule {

	public heroEffectsCMD() {
		super("heroeffects", 0, 1);
	}

	@Override
	public void run(CommandSender sender, String[] args) {
			if (!sender.isOp()) {
				Messaging.send(sender, ChatColor.DARK_RED + "You don't have permission");
				return;
			}
			Hero hero = null;

			if (args.length == 0) {
				if (!(sender instanceof Player)) {
					sender.sendMessage("Specify a player to show their effects.");
					return;
				}
				hero = HeroesAddon.getHeroes().getCharacterManager().getHero((Player) sender);
			}

			if (args.length == 1) {
				Player player = Bukkit.getPlayer(args[0]);
				if (player == null) {
					sender.sendMessage("Offline players are not supported.");
					return;
				}
				hero = HeroesAddon.getHeroes().getCharacterManager().getHero(player);
			}

			if (hero != null) {
				sender.sendMessage(ChatColor.GRAY + "Current effects of " + ChatColor.WHITE + hero.getName() + ChatColor.GRAY + ":");
				for (Effect effect : hero.getEffects()) {
					sender.sendMessage(ChatColor.GRAY + "- Name: " + ChatColor.WHITE +
							effect.getName() + ChatColor.GRAY + ", Type: " + ChatColor.WHITE +
							Lib.getEffectClass(effect));
				}
			}
	}
}
