package com.atherys.heroesaddon.commands;

import com.atherys.heroesaddon.HeroesAddon;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;

public class BindsCMD extends CommandModule {
	public BindsCMD() {
		super("binds", 0, 0);
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command cannot be executed from console");
			return;
		}
		Hero hero = HeroesAddon.getHeroes().getCharacterManager().getHero((Player) sender);
		Map<Material, String[]> bounded = hero.getBinds();
		if (bounded.size() > 0) {
			sender.sendMessage("Current binds:");
			for (Map.Entry binding : bounded.entrySet()) {
				Material tool = (Material) binding.getKey();
				String[] skill = (String[]) binding.getValue();
				sender.sendMessage(" " + tool.toString() + ": " + skill[0]);
			}
		} else {
			sender.sendMessage("You have no bound skills.");
		}
		return;
	}
}
