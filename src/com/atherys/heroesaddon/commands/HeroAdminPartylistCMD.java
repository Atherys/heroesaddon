package com.atherys.heroesaddon.commands;

import com.atherys.heroesaddon.HeroesAddon;
import com.google.common.collect.HashBiMap;
import com.herocraftonline.heroes.characters.Hero;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Set;

public class HeroAdminPartylistCMD extends CommandModule {

	public HeroAdminPartylistCMD() {
		super("HAPartyList", 0, 0);
	}

	@Override
	public void run(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command cannot be executed from console");
			return;
		}
		boolean isParties;
		Player psender = (Player) sender;
		if (args.length == 0) {
			isParties=false;
			HashBiMap<Hero, Set<String>> partyinfo = HashBiMap.create();
			//place all parties here
			for (Player p : Bukkit.getOnlinePlayers()) {
				Hero hero = HeroesAddon.getHeroes().getCharacterManager().getHero(p);
				if (hero.hasParty()) {
					isParties=true;
					if (partyinfo.containsKey(hero.getParty().getLeader())) {
						return;
					} else {
						Hero partylead = HeroesAddon.getHeroes().getCharacterManager().getHero(hero.getParty().getLeader().getPlayer());
						if (!partylead.getParty().getMembers().isEmpty()){
							Set<String> playerName = null;
							for (Hero partyMember:partylead.getParty().getMembers()){
								playerName.add(partyMember.getName());
							}
							partyinfo.put(partylead, playerName);
						}

					}

				}
			}
			if (!isParties){
				psender.sendMessage("Sorry there isn't any Parties on the Server");
				return;
			}else {
				StringBuilder sb = new StringBuilder();
				for (Hero h : partyinfo.keySet()) {
					sb.append(h.getName()).append(" is leader of the party ").append("\n").append(" with the Members:").append(partyinfo.get(h).toString());
					sendMultilineMessage(psender, sb.toString());
				}
			}
		}
	}
		public void sendMultilineMessage(Player player, String message){
			if (player != null && message != null && player.isOnline()){
				String[] s = message.split("\n");
				for (String m : s){
					player.sendMessage(m);
				}
			}
		}
}
