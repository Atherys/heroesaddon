package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.*;

import java.util.HashSet;
import java.util.Set;

public class SmokeEffect extends ExpirableEffect {
    public static final Set<String> hiddenPlayers = new HashSet();
    private final String applyText;
    private final String expireText;

    public SmokeEffect(Skill skill, long duration, String applyText, String expireText,Hero hero) {
        super(skill, "smoke",hero.getPlayer(), duration);
        this.applyText = applyText;
        this.expireText = expireText;

        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.INVIS);
        this.types.add(EffectType.UNTARGETABLE);
        this.types.add(EffectType.MAGIC);
    }

    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        for (Player onlinePlayer : this.plugin.getServer().getOnlinePlayers()) {
            if ((!onlinePlayer.equals(player)) && (!onlinePlayer.hasPermission("heroes.admin.seeinvis"))) {
                onlinePlayer.hidePlayer(player);
            }
        }
        hiddenPlayers.add(player.getName());
        for (Entity entity : player.getNearbyEntities(50.0D, 30.0D, 50.0D)) {
            if ((entity instanceof Monster)) {
                if ((entity.getType().equals(EntityType.SPIDER)) || (entity.getType().equals(EntityType.CAVE_SPIDER))) {
                    Spider spider = (Spider) entity;
                    if (spider.getTarget() != null) {
                        if (spider.getTarget().equals(player)) {
                            spider.setTarget(null);
                        }
                    }
                } else {
                    Monster monster = (Monster) entity;
                    LivingEntity target = monster.getTarget();
                    if (target.equals(player)) {
                        monster.setTarget(null);
                    }
                }
            }
        }
        if ((this.applyText != null) && (this.applyText.length() > 0)) {
            Messaging.send(player, this.applyText, player.getDisplayName());
        }
    }


    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();


        for (Player onlinePlayer : this.plugin.getServer().getOnlinePlayers()) {
            if (!onlinePlayer.equals(player)) {
                onlinePlayer.showPlayer(player);
            }
        }
        hiddenPlayers.remove(player.getName());
        if ((this.expireText != null) && (this.expireText.length() > 0)) {
            Messaging.send(player, this.expireText, player.getDisplayName());
        }
    }
}
