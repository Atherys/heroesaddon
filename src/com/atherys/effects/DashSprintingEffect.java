package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by pumpapa on 01/11/15.
 */
public class DashSprintingEffect extends ExpirableEffect {
    private int amplifier;

    public DashSprintingEffect(Skill skill, String name, long duration, int amplifier,Hero hero) {
        super(skill, name,hero.getPlayer(), duration);
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.SPEED);
        this.amplifier = amplifier;
        if (amplifier > 0) {
            addMobEffect(1, (int) (duration + 1000) / 50, amplifier > 3 ? 3 : amplifier, false);
        }
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        for (PotionEffect effect : player.getActivePotionEffects()) {
            if (effect.getType() == PotionEffectType.SPEED) {
                if (effect.getAmplifier() == amplifier) {
                    player.removePotionEffect(PotionEffectType.SPEED);
                }
                break;
            }
        }
        if (this.isExpired() && player.isSprinting()) {
            long duration = getDuration();
            if (amplifier >= 2) {
                hero.addEffect(new DashSprintingFinalEffect(skill, 3));
            } else {
                amplifier++;
                duration *= 1.8;
                hero.addEffect(new DashSprintingEffect(skill, "DashSprinting" + amplifier, duration, amplifier,hero));
            }
        }
    }

    public int getAmplifier() {
        return amplifier;
    }
}