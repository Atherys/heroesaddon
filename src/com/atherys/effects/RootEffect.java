package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class RootEffect extends PeriodicExpirableEffect {
    private double x;
    private double y;
    private double z;
    private double playerhp;
    private double monsterhp;

    public RootEffect(Skill skill, long duration,Hero hero) {
        super(skill, "Root",hero.getPlayer(), 100L, duration);
        this.types.add(EffectType.ROOT);
        this.types.add(EffectType.HARMFUL);
        this.types.add(EffectType.MAGIC);
    }

    public void applyToMonster(Monster monster) {
        super.applyToMonster(monster);
        Location location = monster.getEntity().getLocation();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.monsterhp = monster.getEntity().getHealth();
    }

    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Location location = hero.getPlayer().getLocation();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        Player player = hero.getPlayer();
        this.playerhp = player.getHealth();
        Messaging.send(player, "You are rooted!");
    }

    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Messaging.send(hero.getPlayer(), "You are no longer rooted!");
    }

    public void tickHero(Hero hero) {
        Player player = hero.getPlayer();
        Location location = player.getLocation();
        if (player.getHealth() >= playerhp) {
            if ((location.getX() != this.x) || (location.getY() != this.y) || (location.getZ() != this.z)) {
                location.setX(this.x);
                location.setY(this.y);
                location.setZ(this.z);
                location.setYaw(player.getLocation().getYaw());
                location.setPitch(player.getLocation().getPitch());
                player.teleport(location);
            }
        } else {
            this.expire();
        }
    }

    public void tickMonster(Monster monster) {
        Location location = monster.getEntity().getLocation();
        if (monster.getEntity().getHealth() == monsterhp) {
            if ((location.getX() != this.x) || (location.getY() != this.y) || (location.getZ() != this.z)) {
                location.setX(this.x);
                location.setY(this.y);
                location.setZ(this.z);
                location.setYaw(monster.getEntity().getLocation().getYaw());
                location.setPitch(monster.getEntity().getLocation().getPitch());
                monster.getEntity().teleport(location);
            }
        } else {
            this.expire();
        }
    }
}