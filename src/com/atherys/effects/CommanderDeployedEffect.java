package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.skill.Skill;

/**
 * Created by computer on 4/10/2016.
 */
public class CommanderDeployedEffect extends Effect {
    private final Hero commander;
    public CommanderDeployedEffect(Skill skill, Hero commander) {
        super(skill, "CommanderDeployedEffect");
        this.types.add(EffectType.BENEFICIAL);
        this.commander = commander;
    }
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
    }
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
    }
    public Hero getCommander(){
        return commander;
    }
}