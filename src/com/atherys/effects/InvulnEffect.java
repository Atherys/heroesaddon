package com.atherys.effects;

import com.atherys.heroesaddon.HeroesAddon;
import com.atherys.heroesaddon.lib.Lib;
import com.atherys.heroesaddon.lib.ParticleEffect;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by pumpapa on 27/09/15.
 */
public class InvulnEffect extends PeriodicExpirableEffect {

    public InvulnEffect(Skill skill, String name, long duration,Hero hero) {
        super(skill, name,hero.getPlayer(),500L ,duration);
        this.types.add(EffectType.INVULNERABILITY);
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.MAGIC);
    }

    public InvulnEffect(Skill skill, long duration,Hero hero) {
        this(skill, "Invuln", duration,hero);
    }

    @Override
    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, "You are invulnerable!");
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, "You are once again vulnerable!");
    }

    @Override
    public void tickMonster(Monster mnstr) {
    }

    @Override
    public void tickHero(Hero hero) {
        Player player = hero.getPlayer();
        for (double[] coords : Lib.playerParticleCoords) {
            Location location = player.getLocation();
            location.add(coords[0], coords[1], coords[2]);
            ParticleEffect.VILLAGER_HAPPY.display(0, 0, 0, 0, 1, location, HeroesAddon.PARTICLE_RANGE);
        }
    }
}