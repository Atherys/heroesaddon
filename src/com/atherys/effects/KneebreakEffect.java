package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Player;

/**
 * Created by pumpapa on 09/08/15.
 */
public class KneebreakEffect extends ExpirableEffect {
    private final String applyText;
    private final String expireText;
    private final long duration;

    public KneebreakEffect(Skill skill, String name, long duration, String applyText, String expireText,Hero hero) {
        super(skill, name,hero.getPlayer(), duration);
        this.applyText = applyText;
        this.expireText = expireText;
        this.duration=duration;
        this.types.add(EffectType.PHYSICAL);
        this.types.add(EffectType.DISPELLABLE);
        this.types.add(EffectType.WEAKNESS);
        this.types.add(EffectType.HARMFUL);
    }

    public KneebreakEffect(Skill skill, long duration, String applyText, String expireText,Hero hero) {
        this(skill, "Kneebreak", duration, applyText, expireText,hero);
    }

    public KneebreakEffect(Skill skill, long duration,Hero hero) {
        this(skill, "Kneebreak", duration, "", "",hero);
    }

    public void applyToMonster(Monster monster) {
        super.applyToMonster(monster);
    }

    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, "You are held down!");
    }

    public void removeFromMonster(Monster monster) {
        super.removeFromMonster(monster);
    }

    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, "You are no longer held down!");
    }
}