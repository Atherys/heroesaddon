package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by pumpapa on 01/11/15.
 */
public class DashSprintingFinalEffect extends PeriodicEffect {
    public DashSprintingFinalEffect(Skill skill, int amplifier) {
        super(skill, "DashSprintingFinal", 5000);
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.SPEED);
        addMobEffect(1, (int) (getPeriod() + 2000 / 50), amplifier, false);
    }

    @Override
    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        for (PotionEffect effect : player.getActivePotionEffects()) {
            if (effect.getType() == PotionEffectType.SPEED) {
                player.removePotionEffect(PotionEffectType.SPEED);
                break;
            }
        }
    }

    @Override
    public void tickHero(Hero hero) {
        if (hero.getPlayer().isSprinting()) {
            reapplyToHero(hero);
        } else {
            hero.removeEffect(this);
        }
    }
}
