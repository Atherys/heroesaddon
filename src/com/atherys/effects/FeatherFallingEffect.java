package com.atherys.effects;

import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.entity.Player;

/**
 * Created by pumpapa on 16/02/16.
 */
public class FeatherFallingEffect extends ExpirableEffect {

    public FeatherFallingEffect(Skill skill, long duration,Hero hero) {
        super(skill, "FeatherFalling",hero.getPlayer(),duration);
        this.types.add(EffectType.BENEFICIAL);
        this.types.add(EffectType.SAFEFALL);
        this.types.add(EffectType.MAGIC);
    }

    public void applyToHero(Hero hero) {
        super.applyToHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, "You have braced for landing!");
    }

    public void removeFromHero(Hero hero) {
        super.removeFromHero(hero);
        Player player = hero.getPlayer();
        Messaging.send(player, "You have lost safefall!");
    }
}
